package datum

import (
	"github.com/jinzhu/gorm"
)

type Role struct {
	gorm.Model
	Name     string `gorm:"not null;unique_index"`
	Entities []EntityRole
}

func (Role) TableName() string {
	return "datum_roles"
}

func NewRole(name string) (role Role, err error) {
	role = Role{Name: name}
	err = nil
	return
}

func (env Environment) GetRole(name string) (role Role, err error) {
	role = Role{}
	if err = env.DB.Where(&Role{Name: name}).First(&role).Error; err != nil {
		return
	}
	err = env.DB.Model(&role).Association("Entities").Find(&role.Entities).Error
	return
}

func (env Environment) GetAllRoles() (roles []Role, err error) {
	roles = []Role{}
	err = env.DB.Find(&roles).Error
	return
}

func (env Environment) GetRoles(filter Role) (roles []Role, err error) {
	roles = []Role{}
	err = env.DB.Where(&filter).Find(&roles).Error
	return
}

func (env Environment) SaveRole(role Role) (err error) {
	old := Role{}
	if err = env.DB.Where(&Role{Name: role.Name}).First(&old).Error; err == nil {
		role.ID = old.ID
		role.CreatedAt = old.CreatedAt
	}
	err = env.DB.Save(&role).Error
	return
}
