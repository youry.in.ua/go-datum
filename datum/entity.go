package datum

import (
	"github.com/jinzhu/gorm"
	"github.com/youryharchenko/guid"
)

type Entity struct {
	gorm.Model
	Uuid  string `gorm:"not null;unique_index"`
	Roles []EntityRole
}

func (Entity) TableName() string {
	return "datum_entities"
}

func NewEntity(roles []Role) (entity Entity, err error) {
	entity = Entity{Uuid: guid.NewString()}
	err = nil
	return
}

func (ent *Entity) AddRole(role Role) (err error) {
	ent.Roles = append(ent.Roles, EntityRole{Entity: *ent, Role: role})
	return nil
}

func (env Environment) GetEntity(uuid string) (entity Entity, err error) {
	entity = Entity{}
	if err = env.DB.Where(&Entity{Uuid: uuid}).First(&entity).Error; err != nil {
		return
	}
	err = env.DB.Model(&entity).Association("Roles").Find(&entity.Roles).Error
	return
}

func (env Environment) GetAllEntities() (entities []Entity, err error) {
	entities = []Entity{}
	err = env.DB.Find(&entities).Error
	return
}

func (env Environment) GetEntities(filter Entity) (entities []Entity, err error) {
	entities = []Entity{}
	err = env.DB.Where(&filter).Find(&entities).Error
	return
}

func (env Environment) SaveEntity(ent Entity) (err error) {
	old := Entity{}
	if err = env.DB.Where(&Entity{Uuid: ent.Uuid}).First(&old).Error; err == nil {
		ent.ID = old.ID
		ent.CreatedAt = old.CreatedAt
	}
	if err = env.DB.Save(&ent).Error; err != nil {
		return
	}

	return
}
