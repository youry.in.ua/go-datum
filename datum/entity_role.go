package datum

import "github.com/jinzhu/gorm"

type EntityRole struct {
	gorm.Model

	EntityId uint   `gorm:"unique_index:idx_entity_role"`
	RoleId   uint   `gorm:"unique_index:idx_entity_role"`
	Entity   Entity `gorm:"ForeignKey:EntityId"`
	Role     Role   `gorm:"ForeignKey:RoleId"`

	//CreatedAt time.Time
	//UpdatedAt time.Time
	//DeletedAt *time.Time
}

func (EntityRole) TableName() string {
	return "datum_entity_roles"
}

/*
func (env Environment) GetEntityRole(ent Entity, role Role) (entityRole EntityRole, err error) {
	entityRole = EntityRole{}
	err = env.DB.Where(&EntityRole{EntityId: ent.ID, RoleId: role.ID}).First(&entityRole).Error
	return
}


func (env Environment) SaveEntityRole(entityRole EntityRole) (err error) {
	err = env.DB.Save(&entityRole).Error
	return
}

func (env Environment) UpdateEntityRoles(entityRole Entity) (err error) {
	for _, role := range ent.Roles {
		if entityRole, err := env.GetEntityRole(ent, role); err == nil {
			err = env.SaveEntityRole(entityRole)
		}
	}
	return
}
*/
