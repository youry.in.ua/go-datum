package datum

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type Environment struct {
	DB *gorm.DB
}

func (env *Environment) Open(db string, conn string, log bool) (err error) {
	if env.DB, err = gorm.Open(db, conn); err == nil {
		env.DB.LogMode(log)
	}
	return
}

func (env *Environment) Schema() (err error) {
	if err = env.DB.AutoMigrate(
		&Role{},
		&Entity{},
		&EntityRole{},
	).Error; err != nil {
		return
	}

	env.DB.Model(&EntityRole{}).AddForeignKey("role_id", "datum_roles(id)", "RESTRICT", "RESTRICT")
	env.DB.Model(&EntityRole{}).AddForeignKey("entity_id", "datum_entities(id)", "RESTRICT", "RESTRICT")

	return
}

func (env *Environment) Close() (err error) {
	err = env.DB.Close()
	return
}
