package datum

import (
	"fmt"
	"testing"
)

func TestEnvironment(t *testing.T) {
	env := Environment{}

	if res := env.Open("postgres", "postgres://test:test@localhost/test?sslmode=disable", true); res != nil {
		t.Error(fmt.Sprintf("Environment open :: error  '%s'", res))
	}

	if res := env.Schema(); res != nil {
		t.Error(fmt.Sprintf("Environment schema :: error  '%s'", res))
	}

	if res := env.Close(); res != nil {
		t.Error(fmt.Sprintf("Environment close :: error  '%s'", res))
	}
}
