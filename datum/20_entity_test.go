package datum

import (
	"fmt"
	"testing"
)

func TestEntity(t *testing.T) {
	env := Environment{}

	if res := env.Open("postgres", "postgres://test:test@localhost/test?sslmode=disable", true); res != nil {
		t.Error(fmt.Sprintf("Environment open :: error  '%s'", res))
	}

	if res := env.Schema(); res != nil {
		t.Error(fmt.Sprintf("Environment schema :: error  '%s'", res))
	}

	ent1, _ := NewEntity([]Role{})

	if res := env.SaveEntity(ent1); res != nil {
		t.Error(fmt.Sprintf("Entity save 1 :: error  '%s'", res))
	}

	if ent2, res := env.GetEntity(ent1.Uuid); res != nil || ent1.Uuid != ent2.Uuid {
		t.Error(fmt.Sprintf("Entity get :: error  '%s'", res))
	} else {
		if res := env.SaveEntity(ent2); res != nil {
			t.Error(fmt.Sprintf("Entity save 2 :: error  '%s'", res))
		} else {
			if ent3, res := env.GetEntity(ent2.Uuid); res != nil || ent2.UpdatedAt == ent3.UpdatedAt {
				t.Error(fmt.Sprintf("Entity get 2 :: error  '%s'", res))
			}
		}
	}

	if ent2, res := env.GetEntity(ent1.Uuid); res != nil || ent1.Uuid != ent2.Uuid {
		t.Error(fmt.Sprintf("Entity get :: error  '%s'", res))
	} else {
		if role2, res := env.GetRole("User"); res != nil {
			t.Error(fmt.Sprintf("Role get :: error  '%s'", res))
		} else {
			if res := ent2.AddRole(role2); res != nil {
				t.Error(fmt.Sprintf("Entity add role :: error  '%s'", res))
			} else {
				fmt.Printf("Roles: %v", len(ent2.Roles))
				if res := env.SaveEntity(ent2); res != nil {
					t.Error(fmt.Sprintf("Entity save 2 :: error  '%s'", res))
				} else {
					if ent3, res := env.GetEntity(ent2.Uuid); res != nil || len(ent3.Roles) != 1 {
						t.Error(fmt.Sprintf("Entity get 2 :: error  '%s', roles: %v", res, ent3.Roles))
					}
				}
			}
		}
	}

	if ents, res := env.GetAllEntities(); res != nil || len(ents) < 1 {
		t.Error(fmt.Sprintf("Entity get all :: error  '%s'", res))
	}

	if ents, res := env.GetEntities(ent1); res != nil || len(ents) != 1 {
		t.Error(fmt.Sprintf("Entity get all :: error  '%s'", res))
	}

	if res := env.Close(); res != nil {
		t.Error(fmt.Sprintf("Environment close :: error  '%s'", res))
	}

}
