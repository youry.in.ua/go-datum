package datum

import (
	"fmt"
	"testing"
)

func TestRole(t *testing.T) {
	env := Environment{}

	if res := env.Open("postgres", "postgres://test:test@localhost/test?sslmode=disable", true); res != nil {
		t.Error(fmt.Sprintf("Environment open :: error  '%s'", res))
	}

	if res := env.Schema(); res != nil {
		t.Error(fmt.Sprintf("Environment schema :: error  '%s'", res))
	}

	role1, _ := NewRole("User")

	if res := env.SaveRole(role1); res != nil {
		t.Error(fmt.Sprintf("Role save 1 :: error  '%s'", res))
	}

	if role2, res := env.GetRole(role1.Name); res != nil || role1.Name != role2.Name {
		t.Error(fmt.Sprintf("Role get :: error  '%s'", res))
	} else {
		fmt.Printf("Entities:  '%v'", len(role2.Entities))
		if res := env.SaveRole(role2); res != nil {
			t.Error(fmt.Sprintf("Role save 2 :: error  '%s'", res))
		} else {
			if role3, res := env.GetRole(role2.Name); res != nil || role2.UpdatedAt == role3.UpdatedAt {
				t.Error(fmt.Sprintf("Role get 2 :: error  '%s'", res))
			}
		}
	}

	if roles, res := env.GetAllRoles(); res != nil || len(roles) < 1 {
		t.Error(fmt.Sprintf("Role get all :: error  '%s'", res))
	}

	if roles, res := env.GetRoles(role1); res != nil || len(roles) != 1 {
		t.Error(fmt.Sprintf("Role get all :: error  '%s'", res))
	}

	if res := env.Close(); res != nil {
		t.Error(fmt.Sprintf("Environment close :: error  '%s'", res))
	}

}
